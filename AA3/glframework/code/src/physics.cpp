﻿#include <imgui\imgui.h>
#include <imgui\imgui_impl_sdl_gl3.h>
#include <glm\gtc\matrix_transform.hpp>
#include <vector>
#include <random>
#include <iostream>

using namespace std;

std::random_device rd;
std::mt19937 gen(rd());
static std::uniform_real_distribution<> dis(-1, 1);
float G = 4.f;
float angle = 1.f;
float sphereMass = 5;
glm::vec3 spherePosition(0.f, 1.f, 0.f);
glm::vec3 gravityAccel(0.f, -9.8f, 0.f);
glm::vec3 windVelocity(0.f,0.f,10.f);
float kWind = 0.5f;
float sphereRadius = 1.f;
float particleMass = 0.01f;
float bounceCo = 0.5f;
float frictionCo = 0.2f;
float ke = 0.1f;
float kd = 0.01f;
int numFibers = 100;
bool playSim = true;
bool sphereColOn = true;
bool useGravity = true;
bool useWind = true;

namespace Box {
	void drawCube();
}
namespace Axis {
	void drawAxis();
}

namespace Sphere {
	extern void updateSphere(glm::vec3 pos, float radius = 1.f);
	extern void drawSphere();
}
namespace Capsule {
	extern void updateCapsule(glm::vec3 posA, glm::vec3 posB, float radius = 1.f);
	extern void drawCapsule();
}
namespace Particles {
	extern const int maxParticles;
	extern void updateParticles(int startIdx, int count, float* array_data);
	extern void drawParticles(int startIdx, int count);
}
namespace Mesh {
	extern const int numCols;
	extern const int numRows;
	extern void updateMesh(float* array_data);
	extern void drawMesh();
}
namespace Fiber {
extern const int numVerts;
	extern void updateFiber(float* array_data);
	extern void drawFiber();
}
namespace Cube {
	extern void updateCube(const glm::mat4& transform);
	extern void drawCube();
}


float get_random(float range)
{
	return dis(rd)*range;
}

struct FiberStraw {
public:
	std::vector<glm::vec3> *previousParticlesPositions = new std::vector<glm::vec3>();
	std::vector<glm::vec3> *particlesPositions = new std::vector<glm::vec3>();;
	std::vector<glm::vec3> *particleVelocities = new std::vector<glm::vec3>();;
	float* flatParticleArray;
	float mass;
	int numParticles = 5;
	FiberStraw(float _mass) { // numParticles = 5
		float randomX = get_random(5.f);
		float randomZ = get_random(5.f);
		for (int i = 0; i < numParticles; i++) {
			if (i == 0) {
				glm::vec3 temp = { randomX, 0.f, randomZ };
				particlesPositions->push_back(temp);
				previousParticlesPositions->push_back(temp);
				particleVelocities->push_back(glm::vec3(0.f));
			}
			else {
				glm::vec3 temp = { randomX, i*0.5f, randomZ };
				particlesPositions->push_back(temp);
				previousParticlesPositions->push_back(temp);
				particleVelocities->push_back(glm::vec3(0.f));
			}
			
			mass = _mass;
			if (i == numParticles -1) {
				flatParticleArray = &particlesPositions->at(0).x;
			}
		}
	}
	void restart() {
		particlesPositions->clear();
		previousParticlesPositions->clear();
		float randomX = get_random(5.f);
		float randomZ = get_random(5.f);
		for (int i = 0; i < numParticles; i++) {
			if (i == 0) {
				glm::vec3 temp = { randomX, 0.f, randomZ };
				//particlesPositions.push_back(temp);
				particlesPositions->push_back(temp);
				previousParticlesPositions->push_back(temp);
				particleVelocities->push_back(glm::vec3(0.f));
			}
			else {
				glm::vec3 temp = { randomX, i * 0.5f, randomZ };
				particlesPositions->push_back(temp);
				previousParticlesPositions->push_back(temp);
				particleVelocities->push_back(glm::vec3(0.f));
			}


			if (i == numParticles -1) {
				flatParticleArray = &particlesPositions->at(0).x;

			}
		}
	}
};

vector<FiberStraw*>fibersVector;

struct Collider {
	glm::vec3 collisionPoint;
	float dPlane;
	glm::vec3 normalVector;

	virtual bool checkCollision(const glm::vec3& prev_pos, const glm::vec3& next_pos) = 0;
	virtual void getPlane(glm::vec3& normal, float& d) = 0;
	void computeCollision(const glm::vec3& old_pos, glm::vec3& new_pos) {
		//...llamar a la funcion get plane 
		getPlane(normalVector, dPlane);
		collisionPoint = (dPlane*(new_pos - old_pos)) + old_pos;
		new_pos = new_pos - (1.f + bounceCo)*(glm::dot(normalVector, new_pos) + dPlane)*normalVector;
		
	}
};

struct PlaneCol : Collider {
	//...
	glm::vec3 collisionPoint;
	glm::vec3 planePoint;
	float dPlane;
	glm::vec3 normalVector;
	bool checkCollision(const glm::vec3& prev_pos, const glm::vec3& next_pos) {
		//glm::vec3 directionLine = next_pos - prev_pos;
		float collisionCheck = (glm::dot(normalVector, prev_pos) + dPlane)*(glm::dot(normalVector, next_pos) + dPlane);
		if (collisionCheck <= 0.f) { // there is collision

			return true;
		}
		else return false;

	}

	void getPlane(glm::vec3& normal, float& d) {
		normal = normalVector;
		d = dPlane;

	}

	void makePlane(const glm::vec3 _planePoint, glm::vec3 _normalVector) {
		normalVector = _normalVector;
		planePoint = _planePoint;
		dPlane = (_normalVector.x*-_planePoint.x) + (_normalVector.y*-_planePoint.y) + (_normalVector.z*-_planePoint.z);
	}
};

struct SphereCol : Collider {
	//...


	float dPlane;
	glm::vec3 normalVector;

	bool checkCollision(const glm::vec3& prev_pos, const glm::vec3& next_pos) { //calcular parametros del plano
		if (sphereColOn == true) {
			float distance = glm::length(spherePosition - next_pos);
			if (distance <= sphereRadius) { //Collision happened

				glm::vec3 collisionPoint;
				glm::vec3 directionLine = glm::normalize(next_pos - prev_pos);
				float a = glm::dot(directionLine, directionLine);
				float b = 2.f*glm::dot(directionLine, prev_pos - spherePosition);
				float c = glm::pow(glm::distance(prev_pos, spherePosition), 2) - glm::pow(sphereRadius, 2);
				float t;
				float t1 = (-b + glm::sqrt(glm::pow(b, 2) - 4 * a*c)) / (2 * a);
				float t2 = (-b - glm::sqrt(glm::pow(b, 2) - 4 * a*c)) / (2 * a);

				if (t1 >= 1.f) {
					t = t2;
				}
				else t = t1;

				//x = x1 + (x2 − x1)t
				collisionPoint.x = prev_pos.x + (next_pos.x - prev_pos.x)*t;
				collisionPoint.y = prev_pos.y + (next_pos.y - prev_pos.y)*t;
				collisionPoint.z = prev_pos.z + (next_pos.z - prev_pos.z)*t;

				normalVector = glm::normalize(collisionPoint - spherePosition);
				dPlane = (normalVector.x*-collisionPoint.x) + (normalVector.y*-collisionPoint.y) + (normalVector.z*-collisionPoint.z);
				return true;
			}
			else return false;

		}
		else return false;

	}
	void getPlane(glm::vec3& normal, float& d) {
		normal = normalVector;
		d = dPlane;

	}
};

std::vector<Collider*> colliders;

struct ForceActuator {

	virtual glm::vec3 computeForce(float mass, const glm::vec3& position) = 0;

};

struct Gravity : ForceActuator {
	glm::vec3 computeForce(float mass, const glm::vec3& position) {
		if (useGravity == true) {
			return gravityAccel*mass;
		}
		else return glm::vec3(0.f);

	}

};

struct WindForce : ForceActuator {
	glm::vec3 computeForce(float mass, const glm::vec3& position) {
		if (useWind == true) {
			return  (kWind * windVelocity * mass);
		}
		else return glm::vec3(0.f);
	}
};

glm::vec3 springforce(const glm::vec3& P1, const glm::vec3& V1, const glm::vec3& P2, const glm::vec3& V2, float L0, float ke, float kd) {
	glm::vec3 p = P1 - P2;
	float modul = sqrt((p.x * p.x) + (p.y * p.y) + (p.z * p.z));
	float term1 = ke * (modul - L0);
	glm::vec3 pNormal = p / modul;

	glm::vec3 v = V1 - V2;
	float dot = glm::dot(kd*v, pNormal);
//	float term2 = kd * dot;
	
	glm::vec3 fromBtoA = { 0.f, 0.f, 0.f };
	fromBtoA +=  -(term1 +  dot)*pNormal;
	return fromBtoA;  
}

std::vector<ForceActuator*> forces;


glm::vec3 computeForces(FiberStraw& fiber, int idx, const std::vector<ForceActuator*>& force_acts) {
	glm::vec3 result = { 0.f, 0.f, 0.f };
	for (int i = 0; i < (int)force_acts.size(); i++) {
		result += force_acts[i]->computeForce(fiber.mass, fiber.particlesPositions->at(idx));
	}
	if (idx % 5 == 0) {
		result += springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx + 1), fiber.particleVelocities->at(idx + 1), 0.5f, ke, kd) +
			springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx + 2), fiber.particleVelocities->at(idx + 2), 0.5f * 2, ke, kd);
	}
	
	else if (idx % 5 == 1)//B
	{
		result += springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx - 1), fiber.particleVelocities->at(idx - 1), 0.5f, ke, kd);
			+ springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx + 1), fiber.particleVelocities->at(idx + 1), 0.5, ke, kd)
			+ springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx + 2), fiber.particleVelocities->at(idx + 2), 0.5f * 2, ke, kd);
	}
	else if (idx % 5 == 2)//c
	{
		//result += springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx - 1), fiber.particleVelocities->at(idx - 1), 0.5f, ke, kd);
		//result += springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx + 1), fiber.particleVelocities->at(idx + 1), 0.5f, ke, kd);
		//result += springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx - 2), fiber.particleVelocities->at(idx - 2), 0.5f*2, ke, kd);
		//result += springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx + 2), fiber.particleVelocities->at(idx + 2), 0.5f*2, ke, kd);
		result += springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx - 1), fiber.particleVelocities->at(idx - 1), 0.5f, ke, kd)
			+ springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx + 1), fiber.particleVelocities->at(idx + 1), 0.5f, ke, kd)
			+ springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx - 2), fiber.particleVelocities->at(idx - 2), 0.5f * 2, ke, kd)
			+ springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx + 2), fiber.particleVelocities->at(idx + 2), 2 * 0.5f, ke, kd);
	}
	else if (idx % 5 == 3)//D
	{
		result += springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx - 1), fiber.particleVelocities->at(idx - 1), 0.5f, ke, kd)
			+ springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx + 1), fiber.particleVelocities->at(idx + 1), 0.5, ke, kd)
			+ springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx - 2), fiber.particleVelocities->at(idx - 2), 0.5f * 2, ke, kd);
	}
	else if (idx % 5 == 4)//E
	{
		result += springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx - 1), fiber.particleVelocities->at(idx - 1), 0.5f, ke, kd)
			+ springforce(fiber.particlesPositions->at(idx), fiber.particleVelocities->at(idx), fiber.particlesPositions->at(idx - 2), fiber.particleVelocities->at(idx - 2), 0.5f * 2, ke, kd);
	}
	//std::cout << result.x << "," << result.y << "," << result.z << endl;*/
	return result;
}

void verlet(float dt, FiberStraw& fiber, const std::vector<Collider*>& colliders, const std::vector<ForceActuator*>& force_acts)
{
	//fer el solver d'una fibra (5 particules), aquest solver es cridara 100 cops
	for (int j = 0; j < (int)fiber.numParticles; j++) {
		//if (i % 5 != 0) {//es a dir, si no es la partcile arrel de la fibra
			glm::vec3 currentPos = fiber.particlesPositions->at(j);// = fiber.particlesPositions->at(i);
			glm::vec3 oldPos = fiber.previousParticlesPositions->at(j);

			glm::vec3 force = computeForces(fiber, j, force_acts);
			fiber.particlesPositions->at(j) = currentPos + (currentPos - oldPos) + ((force / fiber.mass) * dt * dt);
			fiber.previousParticlesPositions->at(j) = currentPos;
			fiber.particleVelocities->at(j) = (fiber.particlesPositions->at(j) - fiber.previousParticlesPositions->at(j)) / dt;

			if (j % 5 == 0) {
				fiber.particlesPositions->at(j) = currentPos;
				fiber.previousParticlesPositions->at(j) = oldPos;
				fiber.particleVelocities->at(j) = glm::vec3(0.f);
			}

			for (int y = 0; y < (int)colliders.size(); y++) {
				if (colliders[y]->checkCollision(fiber.previousParticlesPositions->at(j), fiber.particlesPositions->at(j)) == true) {
					colliders[y]->computeCollision(fiber.previousParticlesPositions->at(j), fiber.particlesPositions->at(j));
				}
			}

		//}

	}


}

// Boolean variables allow to show/hide the primitives
bool renderSphere = true;
bool renderCapsule = false;
bool renderParticles = true;
bool renderMesh = false;
bool renderFiber = true;
bool renderCube = false;


//You may have to change this code
void renderPrims() {
	Box::drawCube();
	Axis::drawAxis();


	if (renderSphere)
		Sphere::drawSphere();
	Sphere::updateSphere(spherePosition, sphereRadius);
	if (renderCapsule)
		Capsule::drawCapsule();

	if (renderParticles) {
		int startDrawingFromParticle = 0;
		int numParticlesToDraw = Particles::maxParticles;
		Particles::drawParticles(startDrawingFromParticle, numParticlesToDraw);
	}

	if (renderMesh)
		Mesh::drawMesh();
	if (renderFiber) {
		for (int i = 0; i < numFibers; i++) {
			Fiber::updateFiber(fibersVector[i]->flatParticleArray);
			Fiber::drawFiber();
		}
	}
		

	if (renderCube)
		Cube::drawCube();
}


void GUI() {
	bool show = true;
	ImGui::Begin("Physics Parameters", &show, 0);

	// Do your GUI code here....
	{	
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);//FrameRate
		ImGui::Checkbox("Play simulation", &playSim);
		if (ImGui::Button("Reset simulation")) {
			for (int i = 0; i < numFibers; i++) {
				fibersVector[i]->restart();
			}
		}
		if (ImGui::InputFloat("particle mass", &particleMass)) {
			//particleSystem.updateMass(particleMass);
		}
		//collision
		if (ImGui::CollapsingHeader("Colision parameters")) {
			ImGui::SliderFloat("Elastic coeficient", &bounceCo, 0.f, 1.f);
			ImGui::SliderFloat("Friction coeficient", &frictionCo, 0.f, 1.f);
		}
		//sphere
		if (ImGui::CollapsingHeader("Sphere parameters")) {
			ImGui::Checkbox("Use sphere collision", &sphereColOn);
			ImGui::InputFloat("Sphere mass", &sphereMass);
			ImGui::InputFloat3("Sphere position", &spherePosition.x);
			ImGui::InputFloat("Sphere radius", &sphereRadius);
		}
		//Forces
		if (ImGui::CollapsingHeader("Force parameters")) {
			ImGui::Checkbox("Use gravity", &useGravity);
			ImGui::InputFloat3("Gravity accel", &gravityAccel.x);
			//ImGui::InputFloat("Positiona coeficient", &G);
		}
		if (ImGui::CollapsingHeader("Spring parameters")) {
			ImGui::InputFloat("spring ke", &ke);
			ImGui::InputFloat("spring kd", &kd);
		}
		
	}
	// .........................
	
	ImGui::End();

	// Example code -- ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
	bool show_test_window = false;
	if(show_test_window) {
		ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);
		ImGui::ShowTestWindow(&show_test_window);
	}
}
glm::vec3 MoveSphere(glm::vec3 spherePos, float radiusMovement, glm::vec3 movementCenter, float angle) {
	glm::vec3 result;
	result.x = movementCenter.x + glm::cos(angle)*radiusMovement;
	result.z = movementCenter.z + glm::sin(angle)*radiusMovement;
	result.y = spherePos.y;
	return result;
}
void PhysicsInit() {
	// Do your initialization code here...
	// ...................................
	//Initialize forces
	Gravity* gravityForce = new Gravity;
	forces.push_back(gravityForce);
	WindForce* windForce = new WindForce;
	forces.push_back(windForce);
		//Initialize Planes
	PlaneCol* bottomPlane = new PlaneCol;
	bottomPlane->makePlane(glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 1.f, 0.f));
	colliders.push_back(bottomPlane);

	PlaneCol* topPlane = new PlaneCol;
	topPlane->makePlane(glm::vec3(0.f, 10.f, 0.f), glm::vec3(0.f, -1.f, 0.f));
	colliders.push_back(topPlane);

	PlaneCol* frontPlane = new PlaneCol;
	frontPlane->makePlane(glm::vec3(0.f, 5.f, 5.f), glm::vec3(0.f, 0.f, -1.f));
	colliders.push_back(frontPlane);

	PlaneCol* backPlane = new PlaneCol;
	backPlane->makePlane(glm::vec3(0.f, 5.f, -5.f), glm::vec3(0.f, 0.f, 1.f));
	colliders.push_back(backPlane);

	PlaneCol* leftPlane = new PlaneCol;
	leftPlane->makePlane(glm::vec3(-5.f, 5.f, 0.f), glm::vec3(1.f, 0.f, 0.f));
	colliders.push_back(leftPlane);

	PlaneCol* rightPlane = new PlaneCol;
	rightPlane->makePlane(glm::vec3(5.f, 5.f, 0.f), glm::vec3(-1.f, 0.f, 0.f));
	colliders.push_back(rightPlane);

	SphereCol* sphere = new SphereCol;
	colliders.push_back(sphere);

	//Initialize fibers
	for (int i = 0; i < numFibers; i++) {
		FiberStraw *fiber = new FiberStraw(0.01f);
		fibersVector.push_back(fiber);
	}

}

void PhysicsUpdate(float dt) {
	// Do your update code here...
	// ...........................
	angle += 1 * dt;
	spherePosition = MoveSphere(spherePosition, 2.f, glm::vec3(0.f, 1.f, 0.f), angle);

	if (playSim == true) {
		//euler(dt, particleSystem, colliders, forces);
		for (int i = 0; i < numFibers; i++) {

	
			verlet(dt, *fibersVector[i], colliders, forces);
		}				
	}

}

void PhysicsCleanup() {
	// Do your cleanup code here...
	// ............................
	forces.clear();
	colliders.clear();
}