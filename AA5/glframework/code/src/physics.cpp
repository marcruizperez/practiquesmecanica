﻿#include <imgui\imgui.h>
#include <imgui\imgui_impl_sdl_gl3.h>
#include <glm\gtc\matrix_transform.hpp>
#include <vector>
#include <random>
#include <iostream>

using namespace std;

std::random_device rd;
std::mt19937 gen(rd());
static std::uniform_real_distribution<> dis(-1, 1);
glm::vec3 positionSphere1, positionSphere2, positionSphere3;
float radiusSphere1, radiusSphere2, radiusSphere3;
float massSphere1, massSphere2, massSphere3;
float friction = 0;
glm::vec3 gravityAccel(0.f, -4.0, 0.f);
const float cooldown = 15.f;
float currentColdown;
bool playSim = false;
bool sphereColOn = false;

namespace Box {
	void drawCube();
}
namespace Axis {
	void drawAxis();
}

namespace Sphere {
	extern void updateSphere(glm::vec3 pos, float radius = 1.f);
	extern void drawSphere();
}
namespace Capsule {
	extern void updateCapsule(glm::vec3 posA, glm::vec3 posB, float radius = 1.f);
	extern void drawCapsule();
}
namespace Particles {
	extern const int maxParticles;
	extern void updateParticles(int startIdx, int count, float* array_data);
	extern void drawParticles(int startIdx, int count);
}
namespace Mesh {
	extern const int numCols;
	extern const int numRows;
	extern void updateMesh(float* array_data);
	extern void drawMesh();
}
namespace Fiber {
extern const int numVerts;
	extern void updateFiber(float* array_data);
	extern void drawFiber();
}
namespace Cube {
	extern void updateCube(const glm::mat4& transform);
	extern void drawCube();
}


float get_random(float range)
{
	return dis(rd)*range;
}

struct Collider {
	glm::vec3 linearMomentum, angularMometum, torque, forces;
	glm::vec3 velocity;
	glm::vec3 centerOfMass;
	glm::vec3 w;
	float mass;

	glm::mat3 inertiaBody;
	glm::mat3 inertia;
	glm::mat3 rotMatrix;

	glm::vec3 normal;
	glm::vec3 colisionPoint;
	virtual bool checkCollision(const glm::vec3& next_pos, float radius) = 0;
};

struct PlaneCol : Collider {

	float dPlane;

	PlaneCol(glm::vec3 _plane, float _d) {
		normal = _plane;
		dPlane = _d;
		mass = 1;
		linearMomentum, angularMometum, torque, forces, velocity, centerOfMass = { 0,0,0 };
		
	}

	bool checkCollision(const glm::vec3& next_pos, float radius) override {
		if (sphereColOn == true) {
			float A = normal.x;
			float B = normal.y;
			float C = normal.z;
			float D = dPlane;// -(A * next_pos.x + B * next_pos.y + C * next_pos.z);
			float term1 = (A * next_pos.x + B * next_pos.y + C * next_pos.z + D);
			float term2 = (A*A + B*B + C*C);
			float distance = term1 / term2;
			if (distance <= radius)
			{
				std::cout << "Collision" << std::endl;
				return true;
			}
			else return false;

		}
		else return false;
	}
};

struct RigidSphere : Collider {
	float radius;
	glm::vec3 oldPos;

	RigidSphere(glm::vec3 _pos, float _radius, float _mass) {
		linearMomentum = { get_random(2), get_random(2), get_random(2) };
		angularMometum = { get_random(2), get_random(2), get_random(2) };
		velocity = { 0, 0, 0 };
		centerOfMass = _pos;
		radius = _radius;
		mass = _mass;
		torque = glm::vec3{ rand() % 8 + (-2), rand() % 8 + 2, rand() % 8 + (-2) };

		inertiaBody = glm::mat3(2 / 5 * mass * radius * radius);

		oldPos = centerOfMass;
	}

	bool checkCollision(const glm::vec3& next_pos, float radius) override {
		float term1 = (next_pos.x - centerOfMass.x)*(next_pos.x - centerOfMass.x) +
			(next_pos.y - centerOfMass.y)*(next_pos.y - centerOfMass.y) +
			(next_pos.z - centerOfMass.z) * (next_pos.z - centerOfMass.z);
		//float distance = glm::distance(next_pos, centerOfMass);
		if (term1 <= (this->radius + radius)*(this->radius + radius)) 
		{
			cout << "Sphere Colisions" << endl;
			colisionPoint.x = centerOfMass.x + (((next_pos.x - centerOfMass.x)*this->radius) / (this->radius + radius));
			colisionPoint.y = centerOfMass.y + (((next_pos.y - centerOfMass.y)*this->radius) / (this->radius + radius));
			colisionPoint.z = centerOfMass.z + (((next_pos.z - centerOfMass.z)*this->radius) / (this->radius + radius));
			return true;
		}
		else return false;
	}

	void setRadiusSphere(float _radius) {
		radius = _radius;
		inertiaBody = glm::mat3(2 / 5 * mass * radius * radius);
	}

	void setMassSphere(float _mass)
	{
		mass = _mass;
		inertiaBody = glm::mat3(2 / 5 * mass * radius * radius);
	}

	void restartSphere() {
		linearMomentum = { get_random(2), get_random(2), get_random(2) };
		angularMometum = { get_random(2), get_random(2), get_random(2) };
		velocity = { 0, 0, 0 };
		centerOfMass = glm::vec3(get_random(2.5) + 2.5f, get_random(2.5) + 2.5f, get_random(2.5) + 2.5f);
	}
};
std::vector<RigidSphere*> spheres;

void euler(float dt, RigidSphere& sph)
{
	sph.forces = glm::vec3{ gravityAccel.x * sph.mass, gravityAccel.y * sph.mass, gravityAccel.z * sph.mass };

	sph.linearMomentum += dt * sph.forces;
	sph.velocity = sph.linearMomentum / sph.mass;

	sph.oldPos = sph.centerOfMass;
	sph.centerOfMass += dt * sph.velocity;
	//orientation
	sph.inertia = sph.rotMatrix * sph.inertiaBody * glm::transpose(sph.rotMatrix);
	sph.angularMometum += dt * sph.torque;
	sph.w = glm::inverse(sph.inertia)*sph.angularMometum;
	sph.rotMatrix += dt * (sph.rotMatrix * sph.w);
}

float computeImpulseCorrection(float massA, glm::vec3 ra, glm::mat3 invIa, float massB, glm::vec3 rb, glm::mat3 invIb, float vrel, float epsilon, glm::vec3 normal) 
{
	float numerador = -(1 + +epsilon) * vrel;
	glm::vec3 rA_x_n = glm::cross(ra, normal);
	glm::vec3 rB_x_n = glm::cross(rb, normal);
	glm::vec3 vecA = invIa * rA_x_n;
	glm::vec3 operationA = glm::cross(vecA, ra);
	glm::vec3 vecB = invIb * rB_x_n;
	glm::vec3 operationB = glm::cross(vecB, rb);
	float denumerador = 1 / massA + 1 / massB + glm::dot(normal, operationA) + glm::dot(normal, operationB);
	return  (numerador / denumerador);
};

void updateColliders(Collider *A, Collider *B)
{
	glm::vec3 pA, pB;
	glm::vec3 normal = A->normal;
	pA = A->velocity + glm::cross(A->w,A->colisionPoint - A->centerOfMass);
	pB = B->velocity + glm::cross(B->w, B->colisionPoint - B->centerOfMass);

	float vrel = glm::dot(normal, (pA - pB));
	float j;
	glm::mat3 invA = inverse(A->inertia);
	glm::mat3 invB = inverse(B->inertia);
	j = computeImpulseCorrection(A->mass, A->w, invA, B->mass, B->w, invB, vrel, friction, normal);
	glm::vec3 JcolA, JcolB;
	JcolA = -j * normal;
	JcolB = -j * normal;
	glm::vec3 torqueImpulseA, torqueImpulseB;
	torqueImpulseA = glm::cross(A->w, JcolA);
	torqueImpulseB = glm::cross(B->w, JcolB);

	A->linearMomentum = A->linearMomentum + torqueImpulseA;
	B->linearMomentum = B->linearMomentum + torqueImpulseB;

	A->angularMometum = A->angularMometum + torqueImpulseA;
	B->angularMometum = B->angularMometum + torqueImpulseB;
}

// Boolean variables allow to show/hide the primitives
bool renderSphere = true;
bool renderCapsule = false;
bool renderParticles = false;
bool renderMesh = false;
bool renderFiber = false;
bool renderCube = false;


//You may have to change this code
void renderPrims() {
	Box::drawCube();
	Axis::drawAxis();


	if (renderSphere)
	{
		for (int i = 0; i < (int)spheres.size(); i++) {
			Sphere::updateSphere(spheres[i]->centerOfMass, spheres[i]->radius);
			Sphere::drawSphere();
		}
	}
	if (renderCapsule)
		Capsule::drawCapsule();

	if (renderParticles) {
	}

	if (renderMesh)
		Mesh::drawMesh();
	if (renderFiber) {
	}

	if (renderCube)
		Cube::drawCube();
}


void GUI() 
{
	bool show = true;
	ImGui::Begin("Physics Parameters", &show, 0);

	// Do your GUI code here....
	{	
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);//FrameRate
		ImGui::Checkbox("Play simulation", &playSim);
		if (ImGui::Button("Reset simulation")) {
			for (int i = 0; i < (int)spheres.size(); i++) {
				spheres[i]->restartSphere();
			}
			currentColdown = cooldown;
		}
		//collision
		if (ImGui::CollapsingHeader("Collision parameters")) {
			ImGui::Checkbox("Spheres Collision", &sphereColOn);
			ImGui::DragFloat("Elasticy coeficient", &friction, 0.05f, 0.f, 1.f);
		}
		//sphere
		if (ImGui::CollapsingHeader("Sphere parameters")) {
			ImGui::Checkbox("Use sphere collision", &sphereColOn);
	//		if (ImGui::CollapsingHeader("Sphere 1")) {

				if (ImGui::InputFloat("Sphere 1 mass", &massSphere1)) {
					spheres[0]->setMassSphere(massSphere1);
				}

				if (ImGui::DragFloat("Sphere 1 radius", &radiusSphere1, 0.1f, 0.f, 4.f)) {
					spheres[0]->setRadiusSphere(radiusSphere1);
				}
	//		}
	//		if (ImGui::CollapsingHeader("Sphere 2")) {
				if (ImGui::InputFloat("Sphere 2 mass", &massSphere2)) {
					spheres[1]->setMassSphere(massSphere2);
				}

				if (ImGui::DragFloat("Sphere 2 radius", &radiusSphere2, 0.1f, 0.f, 4.f)) {
					spheres[1]->setRadiusSphere(radiusSphere2);
				}
	//		}
	//		if (ImGui::CollapsingHeader("Sphere 3")) {
				if (ImGui::InputFloat("Sphere 3 mass", &massSphere3)) {
					spheres[2]->setMassSphere(massSphere3);
				}
				if (ImGui::DragFloat("Sphere 3 radius", &radiusSphere3, 0.1f, 0.f, 4.f)) {
					spheres[2]->setRadiusSphere(radiusSphere3);
				}
	//		}
		}
		//Forces
		if (ImGui::CollapsingHeader("Force parameters")) {
			ImGui::InputFloat("Gravity Force: x", &gravityAccel.x);
			ImGui::InputFloat("Gravity Force: Y", &gravityAccel.y);
			ImGui::InputFloat("Gravity Force: Z", &gravityAccel.z);
			//ImGui::InputFloat("Positiona coeficient", &G);
		}
		
	}
	// .........................
	
	ImGui::End();

	// Example code -- ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
	bool show_test_window = false;
	if(show_test_window) {
		ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);
		ImGui::ShowTestWindow(&show_test_window);
	}
}

glm::vec3 nPlanes[6] = { {1,0,0}, {-1,0,0}, {0,1,0}, {0,-1,0}, {0,0,1}, {0,0,-1} };
float planeValueD[6] = { 5,5,0,10,5,5 };

RigidSphere *sphere1, *sphere2, *sphere3;
PlaneCol *pRight, *pLeft, *pUp, *pBottom, *pForward, *pBack;
std::vector<Collider*> colliders;

void PhysicsInit() {
	radiusSphere1 = 1.f;
	radiusSphere2 = 1.f;
	radiusSphere3 = 1.f;
	massSphere1 = 1.f;
	massSphere2 = 1.f;
	massSphere3 = 1.f;
	sphere1 = new RigidSphere(glm::vec3(get_random(2.5) + 2.5f, get_random(2.5) + 2.5f, get_random(2.5) + 2.5f), radiusSphere1, massSphere1);
	spheres.push_back(sphere1);
	sphere2 = new RigidSphere(glm::vec3(get_random(2.5) + 2.5f, get_random(2.5) + 2.5f, get_random(2.5) + 2.5f), radiusSphere2, massSphere2);
	spheres.push_back(sphere2);
	sphere3 = new RigidSphere(glm::vec3(get_random(2.5) + 2.5f, get_random(2.5) + 2.5f, get_random(2.5) + 2.5f), radiusSphere3, massSphere3);
	spheres.push_back(sphere3);

	colliders.push_back(sphere1);
	colliders.push_back(sphere2);
	colliders.push_back(sphere3);

	pLeft = new PlaneCol(nPlanes[0], planeValueD[0]);
	colliders.push_back(pLeft);
	pRight = new PlaneCol(nPlanes[1], planeValueD[1]);
	colliders.push_back(pRight);
	pBottom = new PlaneCol(nPlanes[2], planeValueD[2]);
	colliders.push_back(pBottom);
	pUp = new PlaneCol(nPlanes[3], planeValueD[3]);
	colliders.push_back(pUp);
	pBack = new PlaneCol(nPlanes[4], planeValueD[4]);
	colliders.push_back(pBack);
	pForward = new PlaneCol(nPlanes[5], planeValueD[5]);
	colliders.push_back(pForward);

}

void PhysicsUpdate(float dt) {

	if (playSim)
	{
		for (int i = 0; i < (int)spheres.size(); i++) {
			euler(dt, *spheres[i]);
			if (sphereColOn)
			{
				for (int j = 0; j < (int)colliders.size(); j++)
				{
					if (colliders[j]->checkCollision(spheres[i]->centerOfMass, spheres[i]->radius) == true)
					{
						updateColliders(spheres[i], colliders[j]);
					}
				}
			}

		}

		//positionSphere1 = sphere1->centerOfMass;
		//radiusSphere1 = sphere1->radius;
		currentColdown -= dt;
		if (currentColdown <= 0) {
			currentColdown = 5.f;
			for (int i = 0; i < 3; i++) {
				spheres[i]->restartSphere();
			}

		}
	}
}

void PhysicsCleanup() {

	delete sphere1;
	delete sphere2;
	delete sphere3;
}