#include <imgui\imgui.h>
#include <imgui\imgui_impl_sdl_gl3.h>
#include <glm\gtc\matrix_transform.hpp>

#include <random>
#include <time.h>
#include <iostream>

namespace Box {
	void drawCube();
}
namespace Axis {
	void drawAxis();
}

namespace Sphere {
	extern void updateSphere(glm::vec3 pos, float radiusSphere);
	extern void drawSphere();
}
namespace Capsule {
	extern void updateCapsule(glm::vec3 posA, glm::vec3 posB, float radiusCapsule);
	extern void drawCapsule();
}
namespace Particles {
	extern const int maxParticles;
	extern void updateParticles(int startIdx, int count, float* array_data);
	extern void drawParticles(int startIdx, int count);
}
namespace Mesh {
	extern const int numCols;
	extern const int numRows;
	extern void updateMesh(float* array_data);
	extern void drawMesh();
}
namespace Fiber {
extern const int numVerts;
	extern void updateFiber(float* array_data);
	extern void drawFiber();
}
namespace Cube {
	extern void updateCube(const glm::mat4& transform);
	extern void drawCube();
}



// Boolean variables allow to show/hide the primitives
bool renderSphere = true;
bool renderCapsule = true;
bool renderParticles = true;
bool renderMesh = false;
bool renderFiber = false;
bool renderCube = false;


const int numParticles = 5000;

float Gravity[3] = { 0.f, - 9.81f, 0.f };
bool useGravity = true;

float cE = 0.5f;
float cF = 0.5f;

bool useSphereCollider = false;
float radiusSphere = 0.5f;
float sphereCenterGUI[3];
glm::vec3 sphereCenter;
float massSphere = 5.f;
bool sphereGravityForce = false;

bool useCapsuleCollider = false;
glm::vec3 capsulePosA; 
float capsuleGuiA[3] = { 0.f, 2.f, 0.f };
glm::vec3 capsulePosB;
float capsuleGuiB[3] = { -2.f,2.f,0.f };
float radiusCapsule;

bool pauseSimulation = false;

bool resetSimulation = false;

void renderPrims() {
	Box::drawCube();
	Axis::drawAxis();


	if (renderSphere)
		Sphere::drawSphere();
	if (renderCapsule)
		Capsule::drawCapsule();

	if (renderParticles) {
		int startDrawingFromParticle = 0;
		Particles::drawParticles(startDrawingFromParticle, numParticles);
	}

	if (renderMesh)
		Mesh::drawMesh();
	if (renderFiber)
		Fiber::drawFiber();

	if (renderCube)
		Cube::drawCube();
}

void GUI() {
	bool show = true;
	ImGui::Begin("Physics Parameters", &show, 0);

	// Do your GUI code here....
	{	
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);//FrameRate
		ImGui::Checkbox("Play/Resume simulation", &pauseSimulation);
		ImGui::Spacing();
		ImGui::Checkbox("Reset Simulation", &resetSimulation);
		ImGui::Separator();
		if (ImGui::TreeNode("Forces"))
		{
			ImGui::Checkbox("Use Gravity", &useGravity);
			ImGui::SliderFloat3("Gravity Force", Gravity, -10.0f, 10.f);
			ImGui::Spacing();
			ImGui::Checkbox("Sphere Gravity Mass", &sphereGravityForce);
			ImGui::SliderFloat("Mass Sphere", &massSphere, 0.f, 100.f);
			ImGui::TreePop();
		}

		ImGui::Separator();
		if (ImGui::TreeNode("Elasticity & Friction"))
		{
			ImGui::SliderFloat("Elasticity", &cE, 0.f, 1.f);
			ImGui::Spacing();
			ImGui::SliderFloat("Friction", &cF, 0.f, 1.f);
			ImGui::TreePop();
		}
		ImGui::Separator();
		if (ImGui::TreeNode("Colliders"))
		{
			ImGui::Checkbox("Use Sphere Collider", &useSphereCollider);
			ImGui::SliderFloat3("Center Sphere", sphereCenterGUI, -10.f, 10.f);
			ImGui::SliderFloat("Radius Sphere", &radiusSphere, 0.f, 15.f);

			ImGui::Spacing();

			ImGui::Checkbox("Use Capsule Collider", &useCapsuleCollider);
			ImGui::SliderFloat3("Center Capsule A", capsuleGuiA, 0.f, 10.f);
			ImGui::SliderFloat3("Center Capsule B", capsuleGuiB, 0.f, 10.f);
			ImGui::SliderFloat("Radius Capsule", &radiusCapsule, 0.f, 10.f);
			ImGui::TreePop();
		}


	}
	// .........................
	
	ImGui::End();

	// Example code -- ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
	bool show_test_window = false;
	if(show_test_window) {
		ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);
		ImGui::ShowTestWindow(&show_test_window);
	}
}

struct Particle {
	glm::vec3 pos;
	glm::vec3 lastPos;
	glm::vec3 velocity;
	glm::vec3 lastVelocity;

	const float mass = 1.f;
};

struct ParticleSystem
{
	Particle myParticles[numParticles];

	ParticleSystem()
	{
		srand(time(NULL));
		for (int i = 0; i < numParticles; i++)
		{
			myParticles[i].pos.x = (static_cast<float>(rand()) / (static_cast<float>(RAND_MAX/10))) - 5.f;
			myParticles[i].pos.y = 9.8f;// static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10));
			myParticles[i].pos.z = (static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 10))) - 5.f;

			myParticles[i].velocity.x = 0;// (static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 2))) - 1.f;
			myParticles[i].velocity.y = (static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 2))) - 1.f;
			myParticles[i].velocity.z = 0;// (static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 2))) - 1.f;

			myParticles[i].lastPos = myParticles[i].pos; // -myParticles[i].velocity * 0.0333f;
			myParticles[i].lastVelocity = myParticles[i].velocity;
		}
	}
};

ParticleSystem *myParticleSystem;

float* convertVector(ParticleSystem& _particles, float  *_arr) {
	for (int i = 0; i < numParticles; i++) {
		_arr[i * 3] = _particles.myParticles[i].pos.x;
		_arr[i * 3 + 1] = _particles.myParticles[i].pos.y;
		_arr[i * 3 + 2] = _particles.myParticles[i].pos.z;
	}

	return _arr;
}

#pragma region COLLIDERS

struct Collider {
	
	glm::vec3 nVector;
	float dist;

	virtual bool checkCollision(const glm::vec3& prev_pos, const glm::vec3& next_pos) = 0;
	virtual void getPlane(glm::vec3& normal, float& d) = 0;

	void computeCollision(const glm::vec3& old_vel, const glm::vec3& old_pos, glm::vec3& new_pos, glm::vec3& new_vel) {
		float dotPos = glm::dot(nVector, new_pos);
		float dotSpeed = glm::dot(nVector, new_vel);
		new_pos = new_pos - (1 + cE) * (dotPos + dist) * nVector;
		glm::vec3 tanVel = new_vel - (nVector * new_vel) * nVector;
		//glm::vec3 tanVel = new_vel - (1 + cE)*(nVector * new_vel) * nVector;
		new_vel = new_vel - (1 + cE) * dotSpeed *  nVector - (cF * tanVel);
	};
};

struct PlaneCol :Collider {

	glm::vec3 nPlane;
	float dPlane;

	PlaneCol(glm::vec3 _plane, float _d) {
		nPlane = _plane;
		dPlane = _d;
	}

	bool checkCollision(const glm::vec3& prev_pos, const glm::vec3& next_pos) {
		float dotPos = glm::dot(nPlane, next_pos);
		float dotLastPos = glm::dot(nPlane, prev_pos);
		if ((dotPos + dPlane) * (dotLastPos + dPlane) <= 0) {
			getPlane(nVector, dist);
			return true;
		}
		else {
			return false;
		}
	}

	void getPlane(glm::vec3& normal, float& d) {
		normal = nPlane;
		d = dPlane;
	}

};

struct SphereCol :Collider {

	glm::vec3 finalCol;
	float dPlane;
	glm::vec3 normalCol;

	void getPlane(glm::vec3& normal, float& d) {
			normal = normalCol;
			d = dPlane;
	}

	bool checkCollision(const glm::vec3& prev_pos, const glm::vec3& next_pos)
	{
		float a = (next_pos.x - prev_pos.x)*(next_pos.x - prev_pos.x)
			+ (next_pos.y - prev_pos.y)*(next_pos.y - prev_pos.y)
			+ (next_pos.z - prev_pos.z)*(next_pos.z - prev_pos.z);

		float b = 2 * ((next_pos.x - prev_pos.x)*(prev_pos.x - sphereCenter.x)
			+ (next_pos.y - prev_pos.y)*(prev_pos.y - sphereCenter.y)
			+ (next_pos.z - prev_pos.z)*(prev_pos.z - sphereCenter.z));
		
		float c = (prev_pos.x - sphereCenter.x)*(prev_pos.x - sphereCenter.x)
			+ (prev_pos.y - sphereCenter.y)*(prev_pos.y - sphereCenter.y)
			+ (prev_pos.z - sphereCenter.z)*(prev_pos.z - sphereCenter.z)
			- (radiusSphere*radiusSphere);

		/*float c = sphereCenter.x * sphereCenter.x + sphereCenter.y * sphereCenter.y
			+ sphereCenter.z * sphereCenter.z + prev_pos.x * prev_pos.x + prev_pos.y *	prev_pos.y
			+ prev_pos.z * prev_pos.z - 2 * (sphereCenter.x * prev_pos.x + sphereCenter.y * prev_pos.y
				+ sphereCenter.z * prev_pos.z) - radiusSphere;*/
		float tmp = (b * b) - (4 * a * c);

		if (tmp >= 0)
		{
			float res1 = (-b + glm::sqrt(tmp)) / (2 * a);
			float res2 = (-b - glm::sqrt(tmp)) / (2 * a);
			float pX = next_pos.x - prev_pos.x;
			float pY = next_pos.y - prev_pos.y;
			float pZ = next_pos.z - prev_pos.z;
			glm::vec3 col1 = { prev_pos.x + pX * res1,  prev_pos.y + pY * res1,  prev_pos.z + pZ * res1 };
			glm::vec3 col2 = { prev_pos.x + pX * res2,  prev_pos.y + pY * res2,  prev_pos.z + pZ * res2 };
			if (glm::distance(next_pos, col1) <= glm::distance(next_pos, col2))
			{
				finalCol = col1;
			}
			else {
				finalCol = col2;
			}

			normalCol = glm::normalize(finalCol - sphereCenter);
			dPlane = ((normalCol.x * finalCol.x) + (normalCol.y * finalCol.y) + (normalCol.z * finalCol.z))*-1;
			
			float dotPos = glm::dot(normalCol, next_pos);
			float dotLastPos = glm::dot(normalCol, prev_pos);
			if ((dotPos + dPlane) * (dotLastPos + dPlane) <= 0) {
				getPlane(nVector, dist);
				return true;
			}
			else {
				return false;
			}

		}
		else {
			return false;
		}
	}
};

struct CapsuleCol : Collider {

	glm::vec3 finalCol;
	float dPlane;
	glm::vec3 normalCol;

	glm::vec3 axisVector;
	glm::vec3 centerCapsule;

	void getPlane(glm::vec3& normal, float& d) {
		normal = normalCol;
		d = dPlane;
	}

	bool checkCollision(const glm::vec3& prev_pos, const glm::vec3& next_pos) {

		axisVector = capsulePosB - capsulePosA;
		//recta r = capsulePOsA + t * axisVector
		//dist(P,r) on r = Q + t * v
		//d = |(P-Q) X V|/|V|
		glm::vec3 PQ = next_pos - capsulePosA;
		float a = glm::length(glm::cross(PQ, axisVector));
		float d = a / glm::length(axisVector);
		if(d <= radiusCapsule) {
		float t = glm::clamp(-(glm::dot(capsulePosA - next_pos, axisVector)) / (glm::length(axisVector)*glm::length(axisVector)), 0.f, 1.f);

			centerCapsule = capsulePosA + t * axisVector;
			float a = (next_pos.x - prev_pos.x)*(next_pos.x - prev_pos.x)
				+ (next_pos.y - prev_pos.y)*(next_pos.y - prev_pos.y)
				+ (next_pos.z - prev_pos.z)*(next_pos.z - prev_pos.z);

			float b = 2 * ((next_pos.x - prev_pos.x)*(prev_pos.x - centerCapsule.x)
				+ (next_pos.y - prev_pos.y)*(prev_pos.y - centerCapsule.y)
				+ (next_pos.z - prev_pos.z)*(prev_pos.z - centerCapsule.z));

			float c = (prev_pos.x - centerCapsule.x)*(prev_pos.x - centerCapsule.x)
				+ (prev_pos.y - centerCapsule.y)*(prev_pos.y - centerCapsule.y)
				+ (prev_pos.z - centerCapsule.z)*(prev_pos.z - centerCapsule.z)
				- (radiusCapsule*radiusCapsule);

			float tmp = b * b - (4 * a * c);

			if (tmp >= 0)
			{
				float res1 = (-b + glm::sqrt(tmp)) / (2 * a);
				float res2 = (-b - glm::sqrt(tmp)) / (2 * a);
				float pX = next_pos.x - prev_pos.x;
				float pY = next_pos.y - prev_pos.y;
				float pZ = next_pos.z - prev_pos.z;
				glm::vec3 col1 = { prev_pos.x + pX * res1,  prev_pos.y + pY * res1,  prev_pos.z + pZ * res1 };
				glm::vec3 col2 = { prev_pos.x + pX * res2,  prev_pos.y + pY * res2,  prev_pos.z + pZ * res2 };
				if (glm::distance(next_pos, col1) <= glm::distance(next_pos, col2))
				{
					finalCol = col1;
				}
				else {
					finalCol = col2;
				}

				normalCol = glm::normalize(finalCol - centerCapsule);
				dPlane = ((normalCol.x * finalCol.x) + (normalCol.y * finalCol.y) + (normalCol.z * finalCol.z))*-1;

				float dotPos = glm::dot(normalCol, next_pos);
				float dotLastPos = glm::dot(normalCol, prev_pos);
				if ((dotPos + dPlane) * (dotLastPos + dPlane) <= 0) {
					getPlane(nVector, dist);
					return true;
				}
				else {
					return false;
				}

			}
			else {
				return false;
			}
		}
	}
};

#pragma endregion

#pragma region FORCES

struct ForceActuator {
	glm::vec3 force;
	virtual glm::vec3 computeForce(float mass, const glm::vec3& position) = 0;
};

struct GravityForce : ForceActuator {

	glm::vec3 computeForce(float mass, const glm::vec3& position) {
		force = glm::vec3(Gravity[0] * mass, Gravity[1] * mass, Gravity[2] * mass);
		return force;
	}
};

struct PositionalGravityForce : ForceActuator {

	glm::vec3 computeForce(float mass, const glm::vec3& position) {
		float term1 = (/*6.674f * glm::pow(10, -11)*/-4.f *  mass * massSphere) / (glm::length(position - sphereCenter)*(glm::length(position - sphereCenter)));
		glm::vec3 term2 = (position - sphereCenter) / glm::length(position - sphereCenter);
		glm::vec3 force = term1 * term2;
		return force;
	}
};

#pragma endregion 

glm::vec3 computeForces(float mass, const glm::vec3& position, const std::vector<ForceActuator*>& force_acts) {
	
	glm::vec3 totalForce = { 0.f,0.f,0.f };
	for (int i = 0; i < 2; i++) {
		totalForce += force_acts[i]->computeForce(mass, position);
	}

	return totalForce;
}

void euler(float dt, ParticleSystem& _particles, const std::vector<Collider*>& _colliders, const std::vector<ForceActuator*>& _force_acts)
{
	for (int i = 0; i < numParticles; ++i)
	{
		_particles.myParticles[i].velocity = _particles.myParticles[i].lastVelocity
			+ (computeForces(_particles.myParticles[i].mass, _particles.myParticles[i].pos, _force_acts)* dt);

		_particles.myParticles[i].pos = _particles.myParticles[i].pos + _particles.myParticles[i].velocity*dt;

		for (int j = 0; j < 8; j++) {
			if (_colliders[j]->checkCollision(_particles.myParticles[i].lastPos, _particles.myParticles[i].pos)) {
				_colliders[j]->computeCollision(_particles.myParticles[i].lastVelocity, _particles.myParticles[i].lastPos,
					_particles.myParticles[i].pos, _particles.myParticles[i].velocity);
			}
		}

		_particles.myParticles[i].lastPos = _particles.myParticles[i].pos;
		_particles.myParticles[i].lastVelocity = _particles.myParticles[i].velocity;
	}
}

float *arrData;

glm::vec3 nPlanes[6] = { {1,0,0}, {-1,0,0}, {0,1,0}, {0,-1,0}, {0,0,1}, {0,0,-1} };
float planeValueD[6] = { 5,5,0,10,5,5 };

PlaneCol *pRight, *pLeft, *pUp, *pBottom, *pForward, *pBack;
SphereCol *mySphereCol;
CapsuleCol *myCapsuleCol;
std::vector<Collider*> colliders;

GravityForce *myGForce;
PositionalGravityForce *myPosGForce;
std::vector<ForceActuator*> forceActs;

void PhysicsInit() {
	myParticleSystem = new ParticleSystem();
	arrData = new float[numParticles*3];

	pLeft = new PlaneCol(nPlanes[0], planeValueD[0]);
	colliders.push_back(pLeft);
	pRight = new PlaneCol(nPlanes[1], planeValueD[1]);
	colliders.push_back(pRight);
	pBottom = new PlaneCol(nPlanes[2], planeValueD[2]);
	colliders.push_back(pBottom);
	pUp = new PlaneCol(nPlanes[3], planeValueD[3]);
	colliders.push_back(pUp);
	pBack = new PlaneCol(nPlanes[4], planeValueD[4]);
	colliders.push_back(pBack);
	pForward = new PlaneCol(nPlanes[5], planeValueD[5]);
	colliders.push_back(pForward);

	mySphereCol = new SphereCol();
	colliders.push_back(mySphereCol);
	myCapsuleCol = new CapsuleCol();
	colliders.push_back(myCapsuleCol);

	myGForce = new GravityForce();
	forceActs.push_back(myGForce);
	myPosGForce = new PositionalGravityForce();
	forceActs.push_back(myPosGForce);
}

void PhysicsUpdate(float dt) {

	if (!pauseSimulation)
	{
		euler(dt, *myParticleSystem, colliders, forceActs);
		convertVector(*myParticleSystem, arrData);
	}
	if (resetSimulation) {
		delete myParticleSystem;
		myParticleSystem = new ParticleSystem();
		resetSimulation = !resetSimulation;
	}
	if (!useGravity) {
		Gravity[0] = 0.f;
		Gravity[1] = 0.f;
		Gravity[2] = 0.f;
	}
	if (!sphereGravityForce) {
		massSphere = 0.f;
	}

	if (useSphereCollider) {
		sphereCenter = { sphereCenterGUI[0], sphereCenterGUI[1], sphereCenterGUI[2] };

	}
	else {
		radiusSphere = 0;
	}

	if (useCapsuleCollider) {
		capsulePosA = { capsuleGuiA[0], capsuleGuiA[1], capsuleGuiA[2] };
		capsulePosB = { capsuleGuiB[0], capsuleGuiB[1], capsuleGuiB[2] };
	}
	else {
		radiusCapsule = 0;
	}
	Particles::updateParticles(0, numParticles, arrData);

	Sphere::updateSphere(sphereCenter, radiusSphere);
	Capsule::updateCapsule(capsulePosA, capsulePosB, radiusCapsule);
}

void PhysicsCleanup() {
	delete myParticleSystem;
	delete[] arrData;

	delete pRight, pLeft, pUp, pBottom, pForward, pBack;
	delete mySphereCol;
	delete myCapsuleCol;

	delete myGForce;
	delete myPosGForce;
}